// JavaScript Document

$(document).ready(function() {

	// images preloader
	for (var vex = 1; vex < 6; vex++) {
		var imgV = '<img src="interactives/intmedia/convex' + vex + '.jpg" />';
		$('#preloader').append(imgV);
	}
	for (var cave = 1; cave < 7; cave++) {
		var imgC = '<img src="interactives/intmedia/concave' + cave + '.jpg" />';
		$('#preloader').append(imgC);
	}


	//======================================
	// INITIALIZE
	//======================================

	var vexArr = {
		1: ['-10','2'],
		2: ['none','none'],
		3: ['30','-2'],
		4: ['20','-1'],
		5: ['16.7','-0.67']
	};
	var caveArr = {
		1: ['-30','3'],
		2: ['none','none'],
		3: ['60','-3'],
		4: ['37.5','-1.5'],
		5: ['30','-1'],
		6: ['26.25','-0.75']
	};
	var imgType = 'convex';
	var slideMax = 5;
	var slideVal = 1;
	var whichArray = vexArr;
	var whichSel = 5;
	var scrollbar = $('.scroll-bar');

	$('.solD').html(whichArray[slideVal][0]);
	$('.solM').html(whichArray[slideVal][1]);
	$('.selD').html(whichSel);

	//======================================
	// TOGGLE SWITCH
	//======================================
	$('.chk_ans').click(function(){
		if ( imgType == 'convex' ) {
			$(this).html('Concave Mirror');
			whichArray = caveArr;
			$('.solD').html(whichArray[slideVal][0]);
			$('.solM').html(whichArray[slideVal][1]);
			imgType = 'concave';
			slideMax = 6;
			whichSel = ( scrollbar.slider('value') * 5 ) + 5;
		} else if ( imgType == 'concave') {
			$(this).html('Convex Lens');
			whichArray = vexArr;
			console.log(slideVal);
			if ( slideVal == 6 ) {
				slideVal = 5;
				scrollbar.slider('option', 'value', 5);
			}
			$('.solD').html(whichArray[slideVal][0]);
			$('.solM').html(whichArray[slideVal][1]);
			imgType = 'convex';
			slideMax = 5;
			whichSel = ( scrollbar.slider('value') * 5 );
		}
		scrollbar.slider('option', 'max', slideMax); // sets slider max value
		$('.imgView').attr('src','interactives/intmedia/' + imgType + slideVal + '.jpg');
		$('.selD').html(whichSel);
	});


	//======================================
	// SLIDER
	//======================================
	function curveSlider() {
		slideVal = scrollbar.slider('value');
		$('.imgView').attr('src','interactives/intmedia/' + imgType + slideVal + '.jpg');
		$('.solD').html(whichArray[slideVal][0]);
		$('.solM').html(whichArray[slideVal][1]);
		if ( imgType == 'convex' ) {
			whichSel = ( scrollbar.slider('value') * 5 );
		} else if ( imgType == 'concave' ) {
			whichSel = ( scrollbar.slider('value') * 5 ) + 5;
		}
		$('.selD').html(whichSel);
	}

	$(function() {
		scrollbar.slider({
			min: 1,
			max: slideMax,
			value: 1,
			step: 1,
			change: curveSlider
		});
	});


});