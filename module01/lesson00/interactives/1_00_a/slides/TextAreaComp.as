﻿/*
* Class: ListComp
* used to customize the list component
* 15th May 2008
* Author: Prashant Raut.
*/
import mx.utils.Delegate;
import mx.events.EventDispatcher;
class TextAreaComp extends MovieClip
{
	private var textAreaDataProvider:String;
	private var componentsArray:Array;
	
	private var textContainer:MovieClip;
	private var mask_mc:MovieClip
	private var scrollBar:MovieClip;
	private var scroller:MovieClip;
	
	private var rootHeight:Number;
	private var rootWidth:Number;
	private var txtHeight:Number;
	private var txtWidth:Number;
	private var _selectedIndex:Number;
	
	private var _selectedItem:String;
	
	private var textArea:TextField;
	private var btn_tf:TextFormat;
	
	public function TextAreaComp()
	{
		trace("TextAreaComp");
		EventDispatcher.initialize(this);
		rootWidth = this._width;
		rootHeight = this._height;
		//listDataProvider = new Array();
		componentsArray = new Array();
		//listDataProvider = ["abc", "def", "ghi", "jkl", "mno", "pqr", "stu", "vwx", "yz", "jkl", "mno", "pqr", "stu", "vwx", "yz"];
		txtHeight = 25;
		txtWidth = rootWidth - 3;
	}
	
	public function addEventListener (event:String, handler:Object):Void
	{
	}
	public function removeEventListener (event:String, handler:Object):Void
	{
	}
	public function dispatchEvent (event:Object):Void
	{
	}
	
	
	private function init()
	{
		setTextFormatForBtn();
		drawTextArea();
	}
	
	private function drawTextArea()
	{
		textContainer = this.createEmptyMovieClip("textContainer", this.getNextHighestDepth());
		componentsArray.push(textContainer);
		textArea = textContainer.createTextField("textArea", textContainer.getNextHighestDepth(), 0, 0, rootWidth-5, rootHeight-10);
		componentsArray.push(textArea);
		//textArea.setNewTextFormat(btn_tf);
		textArea.embedFonts = true;
		textArea.multiline = true;
		textArea.wordWrap = true;
		textArea.autoSize = true;
		textArea.selectable = false;
		textArea.html = true;
		//textArea.htmlText = "Hamari muthhi me aakash sara<br>khulegi jab ye chmakega <u>tara</u><br>Hatheli pe rekhaye hai sab adhri<br>Kisane likhi hai nahi janana hai<br>Khud ke karam se dikhana hai sabako<br>Khud ka panakana ubharana hai khud ka<br>Andhera mitaye vo hi sitara<br>Disha jisase pahechane sansar sara<br>Hamari muthhi me aakash sara<br>khulegi jab ye chmakega <u>tara</u><br>Hatheli pe rekhaye hai sab adhri<br>Kisane likhi hai nahi janana hai<br>Khud ke karam se dikhana hai sabako<br>Khud ka panakana ubharana hai khud ka<br>Andhera mitaye vo hi sitara<br>Disha jisase pahechane sansar sara";
		textArea.htmlText = textAreaDataProvider;
		setMaskTotextContainer();
	}
	
	private function setText(str:String)
	{
		trace("Set text called");
		textAreaDataProvider = str;
	}
	
	/*private function onBtnRelease(evt)
	{
		for(var f:Number=0; f<listDataProvider.length; f++)
		{
			textContainer["btn"+f].rollOverBtn._visible = false;
			textContainer["btn"+f].selected = false;
		}
		evt.rollOverBtn._visible = true;
		evt.selected = true;
		_selectedIndex = parseInt(String(evt._name).substr(3, 2));
		_selectedItem = listDataProvider[_selectedIndex];
		dispatchEvent({type:"click", selectedIndex:_selectedIndex, selectedItem:_selectedItem});
	}*/
	
	private function drawBtn(mov)
	{
		mov.beginFill(0x666666);
		mov.lineStyle(1, 0x999999, 100);
		mov.moveTo(0, 0);
		mov.lineTo(txtWidth, 0);
		mov.lineTo(txtWidth, txtHeight);
		mov.lineTo(0, txtHeight);
		mov.lineTo(0,0);
	}
	
	private function setMaskTotextContainer()
	{
		mask_mc = this.createEmptyMovieClip("mask_mc", this.getNextHighestDepth());
		componentsArray.push(mask_mc);
		drawRect(mask_mc, 0x999999, 0x999999, 0, 0, rootWidth, rootHeight);
		textContainer.setMask(mask_mc);
		
		createScrollBar();
	}
	
	private function drawRect(mov, color, lineColor, x, y, width, height)
	{
		mov.beginFill(color);
		mov.lineStyle(1, lineColor, 100);
		mov.moveTo(x, y);
		mov.lineTo(width, y);
		mov.lineTo(width, height);
		mov.lineTo(x, height);
		mov.lineTo(x,y);
	}
	
	private function createScrollBar()
	{
		scrollBar = this.createEmptyMovieClip("scrollBar", this.getNextHighestDepth());
		componentsArray.push(scrollBar);
		drawRect(scrollBar, 0xAEAEAE, 0xAEAEAE, 0, 0, 3, rootHeight);
		scrollBar._x = rootWidth-3;
		trace('createScrollBar scrollBar._x: ' + scrollBar._x)
		
		drawScroller();
	}
	
	private function drawScroller()
	{
		scroller = this.createEmptyMovieClip("scroller", this.getNextHighestDepth());
		componentsArray.push(scroller);
		var scrollerHeight:Number = (scrollBar._height*mask_mc._height)/textContainer._height;
		drawRect(scroller, 0x000000, 0x000000, 0, 0, 3, scrollerHeight);
		scroller._x = rootWidth-3;
		scroller.onPress = Delegate.create(this, dragScroller);
		scroller.onRelease = scroller.onReleaseOutside = Delegate.create(this, stopDragScroller);
		if(scrollerHeight >= scrollBar._height)
		{
			scroller._visible = false;
			scrollBar._visible = false;
		}
	}
	
	private function dragScroller()
	{
		trace('dragScroller scrollBar._x: ' + scrollBar._x)
		scroller.startDrag(false, scrollBar._x+1, scrollBar._y, scrollBar._x+1,  scrollBar._height-scroller._height);
		scroller.onEnterFrame = Delegate.create(this, moveContainer);
		
	}
	
	private function moveContainer()
	{
		var usedHeightOfScrollBar:Number = scrollBar._height - scroller._height;
		var usedHeightOftextContainer:Number = textContainer._height;
		var scorlltextContainer:Number = (usedHeightOftextContainer*scroller._y*1.1)/scrollBar._height;
		textContainer._y = -4-scorlltextContainer;
	}
	
	private function stopDragScroller()
	{
		scroller.stopDrag();
		delete scroller.onEnterFrame;
	}
	
	private function setTextFormatForBtn()
	{
		btn_tf = new TextFormat();
		btn_tf.font = "Candara";
		btn_tf.size = 15;
		btn_tf.color = 0x000000;
		btn_tf.leading = 5;
		//btn_tf.letterSpacing = 0.75;
	}
	
	private function clearTextArea()
	{
		for(var a:Number=0; a<componentsArray.length; a++)
		{
			componentsArray[a].removeMovieClip();
		}
	}
	
	private function resetScrollBar()
	{
		scroller._y = 0;
		textContainer._y = 0;
	}
}