﻿import mx.utils.Delegate;
import cls.Tooltip;
class cls.Help extends MovieClip
{
	
	public var close_new:MovieClip;
	public var helpbg:MovieClip;
	public var help_btn:MovieClip;
	
	
	function Help()
	{
		help_btn.onRelease = Delegate.create(this, viewHelp);
		help_btn.onRollOver = Delegate.create(this, showHelpTooltip);
		help_btn.onRollOut = Delegate.create(this, hideTooltip);
		/*{
			this._parent.gotoAndStop(2);
			close_new = this.helpbg.close_new;
			trace('close_new: ' + close_new);
			this._parent.addCloseBtn();
		}*/
		var helpKeyListener:Object = new Object();
		helpKeyListener.ref = this;
		helpKeyListener.onKeyDown = function()
		{
			trace(Key.getCode());
			if(Key.getCode() == 72)
			{
				helpKeyListener.ref.viewHelp();
			}
			else if(Key.getCode() == 88)
			{
				helpKeyListener.ref.hideHelp();
			}
		}
		Key.addListener(helpKeyListener);
	}
	
	function viewHelp()
	{
		gotoAndStop(2);
		close_new = this.helpbg.close_new;
		close_new.onRelease = Delegate.create(this, hideHelp);
		close_new.onRollOver = Delegate.create(this, showCloseTooltip);
		close_new.onRollOut = Delegate.create(this, hideTooltip);
		hideTooltip();
	}
	
	private function hideHelp()
	{
		gotoAndStop(1);
		hideTooltip();
		help_btn.onRelease = Delegate.create(this, viewHelp);
		help_btn.onRollOver = Delegate.create(this, showHelpTooltip);
		help_btn.onRollOut = Delegate.create(this, hideTooltip);
	}
	
	public function showHelpTooltip()
	{
		help_btn.gotoAndStop(2);
		Tooltip.show("Help");
	}
	
	public function hideTooltip()
	{
		help_btn.gotoAndStop(1);
		Tooltip.hide();
	}
	public function showCloseTooltip()
	{
		Tooltip.show("Close");
	}
}