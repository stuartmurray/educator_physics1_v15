﻿
var scriptKeyListener:Object = new Object();
scriptKeyListener.onKeyDown = function()
{
	if(Key.getCode() == 84)
	{
		if (!isOpen2 || isOpen2 == undefined) 
		{
			gotoAndPlay(2);
			scriptBtn.enabled = false;
			scriptBtn.gotoAndStop(2);
		}
	}
	else if(Key.getCode() == 88)
	{
		if(isOpen2 == true)
		{
			gotoAndPlay("close");
			scriptBtn.gotoAndStop(1);
			scriptBtn.enabled = true;
		}
	}
	else if(Key.getCode() == 80)
	{
		_parent.slider_new.mcPlay.onRelease();
	}
}
Key.addListener(scriptKeyListener);

import mx.utils.Delegate;
import Tooltip;
this._parent.slider_new.mcPlay.onRollOver = Delegate.create(this, showPlayToolTip);
this._parent.slider_new.mcPlay.onRollOut = Delegate.create(this, hideToolTip);
function showPlayToolTip()
{
	if(!this._parent.slider_new.paused)
	{
		Tooltip.show("Pause");
	}
	else
	{
		Tooltip.show("Play");
	}
}
function hideToolTip()
{
	Tooltip.hide();
}
scriptBtn.onRelease = function()
{
	if (!isOpen2 || isOpen2 == undefined) {
		this._parent.gotoAndPlay(2);
		this.enabled = false;
		this.gotoAndStop(2);
		this._parent.hideToolTip();
	}
}

mcTranscript.close_new.onRelease = function()
{
	this._parent._parent.gotoAndPlay("close");
	this._parent._parent.scriptBtn.gotoAndStop(1);
	this._parent._parent.scriptBtn.enabled = true;

}
mcTranscript.close_new.onRollOver = Delegate.create(this, showCloseToolTip);
mcTranscript.close_new.onRollOut = Delegate.create(this, hideToolTip);
scriptBtn.onRollOver = Delegate.create(this, showScriptToolTip);
scriptBtn.onRollOut = Delegate.create(this, hideToolTip);
function showCloseToolTip()
{
	Tooltip.show("Close");
}
function showScriptToolTip()
{
	Tooltip.show("Transcript");
}