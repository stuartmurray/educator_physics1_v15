﻿class cls.Tooltip{
	private static var t:Number;
	private static var __options:Object = {
		delay:0.5,
		width:200,
		bgColor:0xFFFFDD,
		alpha:85,
		corner:0,
		margin:2,
		font:"Verdana",
		color:0x000000,
		lineColor:null,
		size:11,
		leading:2,
		bold:false,
		align:"left",
		html:true,
		embedFonts:false,
		styleSheet:null,
		follow:false,
		fade:0,
		bgClip:null,
		offset:{x:0, y:22}

	};

	/** Use to change multiple default options globally (accumulative)
	@param options:Object <b>An object with the following optional properties:</b>

	@param options.delay:Number delay in seconds (default: 0.5 second)
	@param options.width:Number maximum width of the tooltip window (default: 200px)
	@param options.bgColor:Color Background color of the tooltip (default: 0xFFFFDD - light yellow)
	@param options.alpha:Number Transparency of the tooltip (default: 85)
	@param optoins.corner:Number The diameter for the rounded corner (default: 5, for sharp corners pass 0)
	@param options.margin:Number (default: 2)
	@param options.font:String (default: "Verdana")
	@param options.color:Color Text and line color, can also accept a string hex value (default 0x000000)
    @param options.lineColor:Color (default: null) Use to override the defualt lineColor (==options.color).
	@param options.size:Number Font size (default: 10)
	@param options.leading:Number line spacing (default: 2)
	@param options.bold:Boolean (default: false)
	@param options.align:String (default: "left")
	@param options.embedFonts:Boolean (default: false)
	@param options.html:Boolean (default: false)
	@param options.styleSheet:TextField.StyleSheet (default: null) Use to extend the default stylesheet in html tooltips
	@param options.follow:Boolean If true the tooltip will follow mouse(default: false)
	@param options.fade:Number or Boolean (default:0 If true or a number>0, the tooltip will fade in and out. The number controls the fade animation chunk. A value of true defaults to 10.
	@param options.bgClip:String (defult:null) If true overrides background and line colour settings and inserts a movie clip Symbol as the background. The mc is stretched to fit the dimensions.
	@param options.offset:Object (default {x:20, y:25}) Distance from mouse. Add a "fixed:true" property to make the tooltip appear in a fixed location on the stage, instead of at the mouse locaction. A fixed tooltip will not adjust to Stage dimensions. Example: {x:0, y:0, fixed:true}
	@return Object
	*/
	public static function get options ():Object {
		return Tooltip.__options;
	}
	public static function set options (value:Object):Void {
		for (var i:String in value) {
			var optionValue:Object = value[i];
			if( optionValue != undefined && optionValue != ""){
				Tooltip.__options[i] = optionValue;
			}
		}
	}
	/**
	Reset all global options to default.
	*/
	public static function resetOptions ():Void{
		__options = {
			delay:0,
			width:200,
			bgColor:0xFFFFDD,
			alpha:85,
			corner:5,
			margin:2,
			font:"Verdana",
			color:0x000000,
			lineColor:null,
			size:11,
			leading:2,
			bold:false,
			align:"left",
			html:true,
			embedFonts:false,
			styleSheet:null,
			follow:false,
			fade:0,
			bgClip:null,
			offset:{x:15, y:15}
		}
	}

	/**
	Use to change a single default option globally (accumulative).
	@param optionName The property name of the option.
	@param optionValue The new value for the option.
	*/
	public static function setOption (optionName:String, optionValue:Object):Void {
		if( optionValue != undefined && optionValue != ""){
			Tooltip.__options[optionName] = optionValue;
		}
	}

	/**
	Show the Tooltip after a delay <br>
	@example
	Example 1: (No options) <br>
	<code>Tooltip.show ("Here is some example text for the static tooltip class"); </code><br>
	Example 2: (One option) <br>
	<code>Tooltip.show ("Here is some example text for the static tooltip class", {delay: 0}); </code><br>
	Example 3: (Many options)<br>
	<code>Tooltip.show ("Here is some example text for the static tooltip class", {delay:1, width:300, bgColor:0xFFFFFF, alpha:80, corner: 10, margiin:5, font: "Times New Roman", size: 14,  color: 0x000099, bold:true, html:true}); </code>
	 @param tipText text to display
	 @param options Use to temporarily override the default/global options
	*/

	public static function show (tipText:String, options:Object):Void {
		Tooltip.hide ();
		if ((tipText == undefined) || tipText == ""){
			return;
		}
		var delay:Number = (options != undefined) && (options.delay != undefined) && (options.delay >= 0) ? options.delay : Tooltip.__options.delay;
		delay *= 1000;
		Tooltip.t = setInterval (Tooltip.doShow, delay, tipText, options);
	}

	/**
	Destroy or hide the Tooltip - if DepthManager is present, than the tooltip will be hidden.<br>
	@example
	* <code>Tooltip.hide()</code>
	*/
	/**
	 *
	 * @usage
	 * @return
	 */
	public static function hide ():Void {
		clearInterval (Tooltip.t);
		var tip:MovieClip = _root.tooltip_mc;
		if (tip.options.fade > 0){
			tip.onEnterFrame = Tooltip.doFadeOut;
		}else{
			Tooltip.kill();
		}
	}

	//Destroy or hide the tooltip;
	private static function kill(){
		var tip:MovieClip = _root.tooltip_mc;
		delete tip.onEnterFrame;
		tip.removeMovieClip ();
		if (_root.tooltip_mc != undefined) {
			//clear the tooltip that still exists on the stage
			tip.clear ();
			tip.background_mc.removeMovieClip();
			delete tip.options;
			tip.tooltip_txt.removeTextField()
			tip._x = 0;
			tip._y = 0;
			tip._alpha = 0;
			tip._visible = false;
		}
	}

	//Display the Tooltip
	private static function doShow (tipText:String, options:Object):Void {
		clearInterval (Tooltip.t);
		Tooltip.kill();
		Tooltip.createTipClip ();
		var defOptions:Object = Tooltip.options;

		if (options == undefined) {
			options = defOptions;
		} else {
			for (var i in defOptions) {
				if (options[i] == undefined || options[i] == "") {
					options[i] = defOptions[i];
				}
			}
		}

		//Style the tooltip
		var myformat:TextFormat;
		var style:TextField.StyleSheet;
		if (options.html) {
			//Use CSS
			if (options.styleSheet instanceof TextField.StyleSheet){
				style = options.styleSheet;
			}else{
				style = new TextField.StyleSheet ();
			}
			var styleObj:Object = new Object ();
			if (typeof (options.color) == "string"){
				styleObj.color = options.color;
			}else{
				//Convert the color object to a hex string
				var clr:String = options.color.toString(16);
				while (clr.length < 6){
					clr = "0" + clr;
				}
				clr = "#" + clr;
				styleObj.color = clr;
			}

			styleObj.fontFamily = options.font;
			styleObj.fontSize = options.size.toString ();
			styleObj.textAlign = options.align;
			if (options.bold) {
				styleObj.fontWeight = "bold";
			}
			styleObj.leading = options.leading;
			style.setStyle ("html", styleObj);
		} else {
			//Create a new textFormat object
			myformat = new TextFormat ();
			//Assign options to the text format
			myformat.color = options.color;
			myformat.font = options.font;
			myformat.size = options.size;
			myformat.bold = options.bold;
			myformat.align = options.align;
			myformat.leading = options.leading;
		}

		//Set the other options
		var corner:Number = options.corner;
		var bgColor:Object = options.bgColor;
		var alpha:Number = options.alpha;

		//Calculate dimensions for a single line
		var w:Number = Math.min (Math.max(100, options.width), Stage.width - 6);
		var h:Number = 100;

		if (options.html && tipText.indexOf("<html") == -1){
			tipText = "<html>" + tipText + "</html>";
		}

		var tip:MovieClip = _root.tooltip_mc;
		tip.createTextField ("tooltip_txt", 2, options.margin, options.margin, 100, h);
		var tooltip_txt:TextField = tip.tooltip_txt;
		tooltip_txt.autoSize = "left";
		tooltip_txt.multiline = true;
		tooltip_txt.wordWrap = false;
		tooltip_txt.embedFonts = options.embedFonts;
		tooltip_txt.selectable = false;
		//Format the Tooltip
		if (options.html) {
			tooltip_txt.html = true;
			tooltip_txt.styleSheet = style;
            tooltip_txt.htmlText = tipText;
		} else {
			tooltip_txt.setNewTextFormat (myformat);
			tooltip_txt.text = tipText;
		}

		//Readjust the width
		var margins:Number = options.margin*2
		if (tooltip_txt._width + margins + 1 > w){
			tooltip_txt.autoSize = false;
			tooltip_txt.wordWrap = true;
			tooltip_txt._width = w;
		}

		h = tooltip_txt.textHeight + 4 + margins;
		w = tooltip_txt._width + margins + 1;
		options.w = w;
		options.h = h;
		tooltip_txt._height = h;
		if (options.bgClip != null){
			var background_mc = tip.attachMovie(options.bgClip, "background_mc", 1, options);
			background_mc._width = w;
			background_mc._height = h;
			background_mc.setOptions(options);
		}else if (options.alpha > 0){
			//Draw the tooltip's shadow
			var sx:Number = 3;
			var sy:Number = 3;
			var sa:Number = options.alpha/10;
			for (var i = 0; i < 3; i++, sx++, sy++) {
				Tooltip.drawRect (sx, sy+2, w, h-5, corner, 0x000000, sa);
			}
			//Draw the tooltip's backround
			var lineColor = options.lineColor != undefined ? options.lineColor : options.color;
			tip.lineStyle (1, lineColor, 50);
			Tooltip.drawRect (0, 0+2, w, h-5, corner, bgColor, options.alpha);
		}
		if (options.offset.fixed){
			options.follow = false;
		}
		tip.options = options;
		Tooltip.placeTip(true);
		tip._visible = true;
		if (options.fade > 0){
			if (options.fade == true){
				options.fade = 10;
			}
			tip._alpha = 0;
			tip.onEnterFrame = Tooltip.doFadeIn;
		}else{
			tip._alpha = 100;
			tip.onEnterFrame = Tooltip.placeTip;
		}
	}

	//Draw a rectangle with the passed coordiantes
	private static function drawRect (x, y, w, h, corner, bgColor, alpha):Void {
		var tip:MovieClip = _root.tooltip_mc;
		tip.beginFill (bgColor, alpha);
			if (corner) {
				//With rounded corners:
				tip.moveTo (x + corner, y);
				tip.lineTo (x + w - corner, y);
				tip.curveTo (x + w, y, x + w, y + corner);
				//micro-curve for a smoother effect
				tip.curveTo (x + w, y+h/2, x+w, y + h - corner);
				tip.curveTo (x + w, y + h, x + w - corner, y + h);
				tip.lineTo (x + corner, y + h);
				tip.curveTo (x, y + h, x , y + h - corner);
				//micro-curve for a smoother effect
				tip.curveTo (x, y+h/2, x, y+corner);
				tip.curveTo (x, y, x + corner, y);
			} else {
				//With sharp corners:
				tip.moveTo (x, y);
				tip.lineTo (x + w, y);
				tip.lineTo (x + w, y + h);
				tip.lineTo (x, y + h);
				tip.lineTo (x, y);
			}
			tip.endFill ();
	}

	//Create a new empty tooltip in the hightest depth
	//(+1 in case the DepthManager is present)
	//Or if the tooltip already exists, make sure it's at the
	//highest depth and swap it up if it isn't
	private static function createTipClip ():Void {
		var tip:MovieClip = _root.tooltip_mcl
		delete tip.onEnterFrame;
		var d:Number = tip.getDepth();
		if (d == undefined ){
			//Create a new tooltip
			d = _root.getNextHighestDepth () + 1;
			tip = _root.createEmptyMovieClip ("tooltip_mc", d);
		} else {
			//Check the difference between the tooltip and the nextHigestDepth
			var d1:Number = _root.getNextHighestDepth ();
			if (d1 > d+1){
				tip.swapDepths(d1);
			}
		}
	}

    //Fade in the tooltip
	private static function doFadeIn(){
		var tip:MovieClip = _root.tooltip_mc;
		if (tip._alpha < 100){
		    tip._alpha +=tip.options.fade;
			//Tooltip.followMouse();
		}else{
			tip._alpha = 100;
			if (tip.options.follow){
				tip.onEnterFrame = Tooltip.placeTip;
			}else{
				delete tip.onEnterFrame;
			}
		}
		Tooltip.placeTip();
	}

	////Fade out the tooltip
	private static function doFadeOut(){
		var tip:MovieClip = _root.tooltip_mc;
		if (tip._alpha > 0){
		    tip._alpha -=tip.options.fade;
		}else{
			tip._alpha = 0;
			delete tip.onEnterFrame;
			Tooltip.kill(true);
		}
		Tooltip.placeTip();
	}

	//Place the tip at the cursor's position or static location and constrain to stage
	private static function placeTip(follow:Boolean){
		var tip:MovieClip = _root.tooltip_mc;
		if (follow || tip.options.follow){
			var w = tip._width;
			var h = tip._height;
			var x:Number;
			var y:Number;
			if (tip.options.offset.fixed){
				x = tip.options.offset.x;
				y = tip.options.offset.y;
			} else {
				x = _root._xmouse + tip.options.offset.x;
				y = _root._ymouse + tip.options.offset.y;
			}
			//Constrain to the boundries of the stage
			if (!tip.options.offset.fixed){
				var r:Number = w + x;
				var l:Number = h + y;
				if (Stage.width < r) {
					//Move the Tooltip to the left
					x -= r - Stage.width;
					//Constrain to stage left
					x = Math.max(0, x);
				}
				if (Stage.height < l) {
					//Move the Tooltip above the cursor
					y -= (h + tip.options.offset.y);
					//Constrain to stage top
					y = Math.max(0, y);
				}
			}
			tip._x = x;
			tip._y = y;
		}
	}
}
