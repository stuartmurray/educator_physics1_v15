﻿import mx.transitions.Tween;
import mx.utils.Delegate;
//import cls.VolumeSlider;
var stage;
var nCurrentPage:Number;
var nTotalPages:Number;
var aPageArray:Array;
var bTweenComplete:Boolean = true;
var bLoadPage:Boolean = true;
var pageSixVisited:Boolean = false;

var volumeLevel:Number = 100;
//mcVol = stage.mcVol;
function fInit ():Void
{
	aPageArray = new Array ("slides/slide01.swf", "slides/slide02.swf", "slides/slide03.swf", "slides/slide04.swf", "slides/slide05.swf", "slides/slide06.swf", "slides/slide07.swf", "slides/slide08.swf", "slides/slide09.swf", "slides/slide10.swf");
	nTotalPages = aPageArray.length;
	mcHL._visible = false;
	for (var i = 1; i<=nTotalPages; i++)
	{
		mcNav.mcMenu["mc"+i].onRelease = function ()
		{
			fNavigate (this._name.substr (2, 2));
		};
	}
	mcNav.mcBack.onRelease = function ()
	{
		if (bTweenComplete)
		{
			fNavigate (Number(nCurrentPage)-1);
		}
	};
	mcNav.mcNext.onRelease = function ()
	{
		if (bTweenComplete)
		{
			fNavigate (Number(nCurrentPage)+1);
		}
	};
	fLoadPage (1);
}

function fLoadPage (numb):Void
{
	//trace(numb);
	if (bLoadPage)
	{
		mcNav.mcNext.gotoAndStop("_disable");
		mcNav.mcNext.enabled = false;
		mcHL._visible = false;
		nCurrentPage = numb;
		fUpdateNavigation ();
		unloadMovie (mcContainer);
		var tempLoadListener:Object = new Object ();
		var tempMCLoader:MovieClipLoader = new MovieClipLoader ();
		tempMCLoader.addListener (tempLoadListener);
		tempMCLoader.loadClip (aPageArray[nCurrentPage-1], mcContainer);
		var tempHelpTween:Tween = new Tween (help_mc, "_alpha", mx.transitions.easing.None.easeNone, 0, 100, 15, false);
		tempLoadListener.onLoadComplete = function ()
		{
			if (bTweenComplete)
			{
				bTweenComplete = false;
				var tempTween:Tween = new Tween (mcPatch, "_alpha", mx.transitions.easing.None.easeNone, 100, 0, 10, false);
				tempTween.onMotionFinished = function ()
				{
					bTweenComplete = true;
					mcNav.btnInv._visible = false;
				};
			}
		};
		tempLoadListener.onLoadInit = function ()
		{
			mcContainer.onEnterFrame = function ()
			{
				//trace (mcContainer._currentframe+"::"+mcContainer._totalframes);
				if (mcContainer._currentframe == mcContainer._totalframes)
				{
					mcNav.mcNext.gotoAndStop("_up")
					mcNav.mcNext.enabled = true;
					mcHL._visible = true;
					trace('nCurrentPage: ' + nCurrentPage);
					trace('nTotalPages: ' + nTotalPages);
					if (nCurrentPage == 1)
					{
						mcNav.mcBack.enabled = false;
						mcNav.mcBack.gotoAndStop ("_disable");
						//mcNav.mcNext.enabled = true;
						//mcNav.mcNext.gotoAndStop ("_up");
					}
					else if (nCurrentPage == nTotalPages)
					{
						//mcNav.mcBack.enabled = true;
						//mcNav.mcBack.gotoAndStop ("_up");
						mcNav.mcNext.gotoAndStop ("_disable");
						mcNav.mcNext.enabled = false;
						mcHL._visible = false;
					}
					delete this.onEnterFrame;
					
				}
			};
		};
		bLoadPage = false;
	}
}
function fCurPageEnded ():Void
{
	mcHL._visible = true;
}
function fNavigate (num):Void
{
	trace(num);
	
	mcNav.btnInv._visible = true;
	if (bTweenComplete)
	{
		bTweenComplete = false;
		var tempTween:Tween = new Tween (mcPatch, "_alpha", mx.transitions.easing.None.easeNone, 0, 100, 10, false);
		tempTween.onMotionFinished = function ()
		{
			bTweenComplete = true;
			bLoadPage = true;
			fLoadPage (num);
		};
	}
}


/*
**********************************Adding functionality to keyBoard*************************
*/
var keyListener:Object = new Object();
keyListener.onKeyDown = function()
{
	if(Key.getCode() == 39)
	{
		if(mcNav.mcNext.enabled == true)
		{
			mcNav.mcNext.onRelease();
		}
	}
	if(Key.getCode() == 37)
	{
		if(mcNav.mcBack.enabled == true)
		{
			mcNav.mcBack.onRelease();
		}
	}
}
Key.addListener(keyListener);
/*******************************************************************************************/


function fFadeIn ():Void
{
	//trace ("fFadeIn");
	mcPatch.gotoAndPlay ("fadein");
}
function fFadeOut ():Void
{
	//trace ("fFadeOut");
	mcPatch.gotoAndPlay ("fadeout");
}
function fUpdateNavigation ():Void
{
	if (nCurrentPage>=1 && nCurrentPage<=3)
	{
		mcNav.mcMenu._x = -6;
		fUpdatePointer (nCurrentPage);
	}
	else if (nCurrentPage==4)
	{
		mcNav.mcMenu._x = -60;
		fUpdatePointer (3);
	}
	else if (nCurrentPage==5)
	{
		mcNav.mcMenu._x = -120;
		fUpdatePointer (3);
	}
	else if (nCurrentPage==6)
	{
		mcNav.mcMenu._x = -175;
		fUpdatePointer (3);
	}
	else if (nCurrentPage==7)
	{
		mcNav.mcMenu._x = -235;
		fUpdatePointer (3);
	}
	else
	{
		mcNav.mcMenu._x = -290;
		fUpdatePointer (nCurrentPage-5);
	}
	fUpdateButtons ();
}
function fUpdatePointer (num):Void
{
	mcNav.mcPointer.gotoAndStop (num);
}
function fUpdateButtons ():Void
{
	if (nCurrentPage == 1)
	{
		mcNav.mcBack.enabled = false;
		mcNav.mcBack.gotoAndStop ("_disable");
		//mcNav.mcNext.enabled = true;
		//mcNav.mcNext.gotoAndStop ("_up");
	}
	else if (nCurrentPage == nTotalPages)
	{
		//mcNav.mcBack.enabled = true;
		//mcNav.mcBack.gotoAndStop ("_up");
		mcNav.mcNext.enabled = false;
		mcNav.mcNext.gotoAndStop ("_disable");
	}
	else
	{
		mcNav.mcBack.enabled = true;
		mcNav.mcBack.gotoAndStop ("_up");
		//mcNav.mcNext.enabled = true;
		//mcNav.mcNext.gotoAndStop ("_up");
	}
}
fInit ();
