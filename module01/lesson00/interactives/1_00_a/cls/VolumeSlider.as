﻿import mx.utils.Delegate;
import mx.events.EventDispatcher;
class cls.VolumeSlider extends MovieClip
{
	private var mcVol:MovieClip;
	public var isOpen:Boolean = false;
	public var percent:Number = 0;
	
	public function VolumeSlider ()
	{
		//trace (mcVol);
		fInit (100);
	}
	
	public function fInit (vol):Void
	{
		EventDispatcher.initialize (this);
		fAddListener ();
	}
	
	public function addEventListener (event:String, handler:Object):Void
	{
	}
	
	public function removeEventListener (event:String, handler:Object):Void
	{
	}
	
	public function dispatchEvent (event:Object):Void
	{
	}
	
	private function fAddListener ():Void
	{
		mcVol.mcSlide.onPress = function ()
		{
			this.startDrag (false, -7.5, this._parent.mcSlideBar._y, -7.5, this._parent.mcSlideBar._y+this._parent.mcSlideBar._height);
			this.onEnterFrame = function ()
			{
				percent = Number (Number (this._y)*100)/this._parent.mcSlideBar._height;
				//this._parent._parent._parent.volume = percent;
				//trace(Math.abs(percent-100));
				this._parent._parent.fDispatch(percent);
			};
			
		};
		mcVol.mcSlide.onRelease = mcVol.mcSlide.onReleaseOutside=function ()
		{
			delete this.onEnterFrame;
			stopDrag ();
		};
	}
	
	public function fDispatch(percent):Void
	{
			dispatchEvent({type:"SliderChanged", target:this._parent , percent:percent});
	}
		
	public function open ():Void
	{
		gotoAndPlay ("open");
		isOpen = true;
	}
	
	public function close ():Void
	{
		gotoAndPlay ("close");
		isOpen = false;
	}
}
