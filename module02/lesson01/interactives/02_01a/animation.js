// JavaScript Document
$(document).ready(function(){
	
	// INITIALIZE
	$('#start').css('display','block');
	
	var carInitHeight = '85px';
	var carInitWidth = '136px';
	var carInitTop = '175px';
	var carInitLeft = '245px';
	var signInitHeight = '140px';
	var signInitWidth = '61px';
	var signInitTop = '102px';
	var signInitLeft = '489px';
	var whichPop = '1';
	
	// CAR ANIMATION
	function carAnimation() {
		$('#start').fadeOut(50);
		$('#car_sm').animate({
    		width: '34px',
			height: '21px',
			top: '175px',
			left: '285px'
		}, {
			duration: 4000,
			complete: function(){
				$('#reset').fadeIn(100);
				$('#pop_' + whichPop).delay(500).fadeIn(500);
				$('#coverit').delay(500).fadeIn(500);
				if ( whichPop == 2 ) {
					$('#next').fadeOut(100);
				} else {
					$('#next').fadeIn(100);
				}
			}
		});
		
	}
	
	function resetCar() {
		$('#car_sm').css({
			'height': carInitHeight,
			'width': carInitWidth,
			'top': carInitTop,
			'left': carInitLeft
		});
	}
	
	// STOP SIGN ANIMATION
	function signAnimation() {
		$('#stopsign').animate({
    		width: '15px',
			height: '35px',
			top: '162px',
			left: '336px'
		}, {
			duration: 4000
		});
	}
	
	function resetSign() {
		$('#stopsign').css({
			'height': signInitHeight,
			'width': signInitWidth,
			'top': signInitTop,
			'left': signInitLeft
		});
	}
	
	
	// START BUTTON CLICKED
	$('#start').click(function(){
		carAnimation();
	});
	
	//NEXT BUTTON CLICKED
	$('#next').click(function(){
		$('#next').fadeOut(100);
		resetCar();
		whichPop = '2';
		$('#stopsign').fadeIn(100);
		setTimeout(carAnimation, 1000);
		setTimeout(signAnimation, 1000);
	});
	
	// RESET BUTTON CLICKED
	$('#reset').click(function(){
		$('#reset').fadeOut(100);
		$('#stopsign').fadeOut(100);
		$('#start').fadeIn(100);
		$('#next').fadeOut(100);
		whichPop = '1';
		resetCar();
		resetSign();
	});
	
	
	// POPUPS
	
	// add in close button
	var closeBtn = $(document.createElement('div')).attr('class','closeit').html('X');
	$('.tl_popup').append(closeBtn);
	
	// close popup function
	$('.closeit, #coverit').click(function(){
		$('.tl_popup').hide();
		$('#coverit').hide();
	});
	
	
});