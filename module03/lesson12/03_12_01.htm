<!DOCTYPE html>
<html lang="en">
<head>
	<title>3.12 Force Diagram Tutorial and Free Body</title>
	<meta charset="utf-8">
	<script type="text/javascript">
		var pg_csslibs = []; // CSS Libs for this Page
		var pg_jslibs = []; // JS Libs for this Page
	</script>
	<script src="../../global/js/settings.js" id="settingJS" type="text/javascript"></script>
</head>
<body>
<div id="pageContent">
	<!-- CONTENT GOES HERE -->
	<p><span class="darkGreenText"><strong>Purpose:</strong></span> To learn how to draw free bodies</p>

	<p>
		<span class="darkGreenText"><strong>Introduction:</strong></span> As a first step in learning about free-body diagrams, you must distinguish them from a host of other drawings employed in many books in which forces, position, and velocities are all indicated on a single drawing. In contrast, a free-body diagram shows all the forces acting on a
		<span class="redText"><strong>single, isolated body.</strong></span> Only forces should be entered on a free-body diagram. The idea behind a free-body diagram is that the motion of an object is determined by the forces acting on the object. Indicating
		<strong><span class="redText">all</span></strong> of the forces acting on a single body makes it possible to discuss and determine the behavior of that body without referring to any of the objects exerting the forces.
	</p>

	<p>When drawing a free-body diagram, it is customary to draw a
		<span class="redText"><strong>point</strong></span> to represent the body. This point should be drawn
		<strong><span class="redText">away</span></strong> from any other situation or diagram. On this drawing, indicate each force acting on the body as a directed line segment with the tail end of the vector starting at the point. The direction of the arrow should be in the same direction as the force. Whenever possible, the length of the vector should be drawn to be roughly proportional to the size of the force. Each force in the diagram should be
		<span class="redText"><strong>clearly labeled</strong></span> and distinguishable from all the other forces present in the physical situation.
	</p>

	<p>In some cases, it is helpful to include a set of axes indicating component directions on the free-body diagram. These axes can be placed anywhere on the diagram but are usually placed with the origin located at the point representing the center of the object. Also, these axes can be oriented in any way. Usually, the orientation is chosen to make it easier or more convenient to find components of the forces.</p>

	<p>Sometimes, for illustration purposes, the object is drawn as having spatial extent, such as a block or square, with all the forces drawn from a point at the center of the body. When drawing the object, it is important to preserve the object's orientation.</p>

	<p>
		<span class="darkGreenText"><strong>Materials:</strong></span> Ruler, protractor, calculator, graph paper is optional
	</p>

	<p><span class="darkGreenText"><strong>Procedure:</strong></span></p>
	<ol>
		<li>
			<p>The force diagram consists of a simple drawing of an object accompanied by vectors to represent each force acting on that object. Many students, however, have difficulty learning how to draw these diagrams. They also have trouble describing the forces they have drawn.</p>

			<p>Some common difficulties inlcude:</p>
			<ul>
				<li>
					<p>Not being able to identify all of the forces present in a given situation</p>
				</li>
				<li>
					<p>Proposing nonexistent forces</p>
				</li>
				<li>
					<p>Suggesting incorrect directions for forces</p>
				</li>
				<li>
					<p>Not being able to correctly identify the object on which the force is acting</p>
				</li>
				<li>
					<p>Not being able to correctly identify the cause of the force</p>
				</li>
			</ul>
		</li>
		<li>
			<p>Review Newton's Third Law. Lack of understanding of the Third Law can be a further "roadblock" to learning this skill.</p>
		</li>
		<li>
			<p>Here is an example of a force diagram showing the forces acting on an apple that is motionless on a table:</p>
			<img src="images/3_12_a.gif" alt="force diagram" class="pull-right top-flush" data-width="" data-copyright="&copy; Paul Hewitt">

			<p>The upward vector represents the "normal" force of support acting on the apple. The table is pushing upward on the apple.</p>

			<p> The downward vector represents the "weight" of the apple. The weight is caused by the gravitational interaction between the Earth and the apple.</p>

			<p> Notice that the force diagram shows the following:</p>

			<p> a simple picture of the object, arrows to represent the forces acting on the object, and the correct name of each force. In this case, you could just use a circle to represent the apple. </p>
		</li>
		<li>
			<p>Notice that the upward normal force and the downward weight seem to be the same magnitude, but are opposite in direction. You might think that these forces are examples of Newton's Third Law. However, this is not the case. Remember that Newton's Third Law states that if object A exerts a force on object B, then object B exerts an equal and opposite force on object A. There are two objects (A and B) mentioned in the Third Law. In our example, we only have one object (the apple). In the Third Law, the forces mentioned act on different objects (A and B), but in our example, the forces act on one object (the apple).</p>
		</li>
	</ol>
	<hr/>
	<p>
		<span class="darkGreenText"><strong>Examples:</strong></span></p>

	<div class="panel-group" id="accordion">
		<div class="panel panel-default">
			<div class="panel-heading" data-toggle="collapse" data-parent="#accordion" data-target="#collapseOne">
				<h4 class="panel-title">Example One</h4>
			</div>
			<div id="collapseOne" class="collapse">
				<div class="panel-body">
					<img src="images/3_12_b.gif" alt="free-body diagram with a tension force up and a weight force down" class="pull-left top-flush" data-width="" data-copyright="">

					<p><span class="darkGreenText">A <span class="redText">block</span>
          is suspended from a mass-less
          rope.<br></span></p>

					<p><span class="darkGreenText">The forces often applied by means of cables
          or ropes that are used to pull an object are given the
          name <span class="redText">tension forces
          (T)</span>.</span></p>

					<p><span class="darkGreenText">The</span> downward vector represents the
          <span class="darkGreenText"><span class="redText">"weight" (F<sub>g</sub>)</span> of the object. The weight is caused by the
          gravitational interaction between the Earth and the
          object.</span></p>
				</div>
			</div>
		</div>
		<!--End item #1-->
		<div class="panel panel-default">
			<div class="panel-heading" data-toggle="collapse" data-parent="#accordion" data-target="#collapseTwo">
				<h4 class="panel-title">Example Two</h4>
			</div>
			<div id="collapseTwo" class="collapse">
				<div class="panel-body">
					<img src="images/3_12_c.gif" alt="free-body diagram with a normal force up and a weight force down" class="pull-left top-flush" data-width="" data-copyright="">

					<p><span class="darkGreenText">A <span class="redText">book</span>
          rests on a
          table.</span></p>

					<p>The upward pressing force is called the <span class="darkGreenText"><span class="redText">normal force
          (F<sub>N</sub>)</span>. The term
          normal is taken from math, and it means
          perpendicular.</span></p>

					<p>The downward vector represents the
						<span class="redText">"weight" (F<sub>g</sub>)</span> of the object. The weight is caused by the gravitational interaction between the Earth and the object.
					</p>
				</div>
			</div>
		</div>
		<!--End item #2-->
		<div class="panel panel-default">
			<div class="panel-heading" data-toggle="collapse" data-parent="#accordion" data-target="#collapse3">
				<h4 class="panel-title">Example Three</h4>
			</div>
			<div id="collapse3" class="collapse">
				<div class="panel-body">
					<img src="images/3_12_d.gif" alt="free-body diagram with a normal force up, weight force down, frictional force toward the left, and an applied force toward the right" class="pull-left top-flush" data-width="" data-copyright="">

					<p>A <span class="darkGreenText"><span class="redText">block</span>
          is pulled with constant velocity
          along a horizontal surface having
          friction.<br></span></p>

					<p>The force that opposes motion or pending motion is the <span class="darkGreenText"><span class="redText">frictional force
          (F<sub>f</sub> )</span>. The
          direction of the friction force is opposite the direction
          of motion or pending motion. This force arises from the
          irregularities of two surfaces being pressed together.
          Two factors that affect the amount of friction are the
          pressing forces between the two surfaces and the nature
          of the two surfaces.</span></p>

					<p>The pressing force is called the <span class="redText">normal force
          (F<sub>N</sub>)</span>. The term normal is taken from math, and it means perpendicular. The ratio of the frictional force to the normal force is called the coefficient of friction (<img src="images/mu.gif" alt="mu" width="9" height="13" class="">).
					</p>

					<p>The equation for friction is as follows:</p>

					<p>
						<span class="darkGreenText">F<sub>f</sub> = <img src="images/mu.gif" alt="mu" width="9" height="13" class="">F<sub>N</sub></span>
					</p>

					<p>F<sub>f</sub> is the frictional force<br> F<sub>N</sub> is the normal force
						<span class="darkGreenText"></span>

					<p><img src="images/mu.gif" alt="mu" width="9" height="13" class=""> is the coefficient of friction
					</p>

					<p class="text-left">Friction is undesirable when it causes the wear of metal parts and excessive heat. Friction can be reduced by changing the surfaces, lubricating the surfaces, or changing the type of friction. Rolling friction is less than kinetic friction, which is less than static friction.</p>

					<p><span class="darkGreenText">Friction is essential for walking, braking,
          and keeping objects at rest.</span></p>

					<p>The coefficient between any two surfaces is a constant if the type of friction remains the same; then, as the normal force between the two surfaces increases, the frictional force between the two surfaces increases proportionally.</p>

					<div class="center-block">
						<p>
							<span class="darkGreenText"><img src="images/3_12_e.jpg" alt="kinetic frictional force versus normal force graph" width="338" height="237" class=""></span>
						</p>

						<p class="text-left">The slope of this graph is the coefficient of kinetic friction between the two surfaces.</p>

						<p class="text-left"><img src="images/3_12_f.jpg" alt=" " width="218" height="172" class=""></p>

						<p class="text-left">The coefficient between any two surfaces is a constant. This coefficient changes as the friction goes from a static form to a kinetic form. Notice the graph below shows that as the force applied to an object at rest increases, the static friction increases until the object moves. The purple arrow is pointing to where the object goes from rest (static friction) to moving with a constant velocity (kinetic friction). The kinetic friction is normally less than the static friction. When the object is at rest, we say it is in static equilibrium; and when it is moving at a constant velocity, we say it is in translational equilibrium. Notice that the surface area of the object and the rate at which it moves is not taken into consideration because these factors do not normally have an effect on the amount of friction between the two surfaces.</p>

						<p class="text-center">.<img src="images/3_12_g.jpg" alt="frictional resistance force versus applied force on an object; arrow pointing to the change in the type of frictional force" width="337" height="219" class="">
						</p>

						<p class="text-left">Liquids and gases also resist the passage of a solid object. As the object moves faster through the fluid, the resistance to its motion, the drag, also increases. When the fluid friction is equal to the weight of the object as it falls, the object ceases to accelerate. This state of translational equilibrium (falling but not accelerating) is referred to as terminal speed or terminal velocity.</p>
					</div>
				</div>
			</div>
		</div>
		<!--End item #3-->
		<div class="panel panel-default">
			<div class="panel-heading" data-toggle="collapse" data-parent="#accordion" data-target="#collapse4">
				<h4 class="panel-title">Example Four</h4>
			</div>
			<div id="collapse4" class="collapse">
				<div class="panel-body">
					<img src="images/3_12_h.gif" alt="free-body diagram frictional drag force up and weight down" class="pull-left top-flush" data-width="" data-copyright="">

					<p><span class="darkGreenText">A <span class="redText">feather</span>
          experiences terminal velocity as
          it fall through the air.<br></span></p>

					<p>Liquids and gases also resist the passage of a solid object; this fluid resistance <span class="darkGreenText"><span class="redText">(F<sub>drag</sub>)</span>
          is due to the collision of the
          solid with the molecules that make up the fluid. As the
          object moves faster through the fluid, the resistance to
          its motion, the drag, also increases. When the fluid
          friction is equal to the weight of the object as it
          falls, the object ceases to accelerate. This state of
          translational equilibrium (falling but not accelerating)
          is referred to as <span class="redText">terminal
          speed or terminal velocity</span>.</span></p>

					<p><span class="darkGreenText">The</span> downward vector represents the
          <span class="darkGreenText"><span class="redText">"weight" (F<sub>g</sub>)</span> of the object. The weight is caused by the
          gravitational interaction between the Earth and the
          object.</span></p>
				</div>
			</div>
		</div>
		<!--End item #4-->
	</div>
	<!--End Accordion-->
	<hr/>
   

   <span class="extraSmallBlackArialText">Source: &copy; 1997
        Paul Hewitt, modified by FLVS</span><!-- END CONTENT -->
</div>
</body>
</html>
