// ----------------------------------------------------------------------
// -- COURSE SPECIFIC JS:
// ----------------------------------------------------------------------
// -- NOTE: This is where you can add anything you need to do specifically to the course homepage, it will load lastly.
// -- ABOUT: THis file will over-ride everything else, if you need to customize
// -- AUTHOR: Vaughn Thompson - WDS
// ======================================================================

$( 'div #menu' ).remove();

function createIndexLessons(){
	
	// add in lesson number to breadcrumb
	if ("undefined" == typeof isexternal){
		$('.breadcrumbs_module_num').html(FLVS.Sitemap.module[current_module].num)
		$('.breadcrumbs_num').html(FLVS.Sitemap.module[current_module].lesson[current_lesson].num);
		$('.breadcrumbs_num, .breadcrumbs_lesson').css('font-weight','normal');
		$('.breadcrumbs_module_num').css('font-weight','bold');
	}
	
	// Create Popup Menu
	createMenuLessons();
	
	// Event for Menu Button
	$('.menubtn, .menubtn_mobile').click(function(){
		$('.navnoheader').removeClass('navnohover');
		$('.nav_menu_lessons').hide();
				
		if(!$('#nav_menu').is(':visible')){
			$('body').append('<div class="menu_backdrop">&nbsp;</div>');
			$('.menu_backdrop').click(function(){
				$('#nav_menu').fadeToggle('fast');
				$(this).remove();
			});
		} else {
			$('.menu_backdrop').remove();
		}
				
		$('#nav_menu').fadeToggle('fast');
		
		//in case home nav menu is open
		$('.opened').fadeOut();
		$('.div_lessons').removeClass('opened');
		$('#lesson_menu').fadeOut();
		$('.lesson_backdrop').fadeOut();
				
	});
		
	// Event for Showing Menu Lessons
	$('.modlink').click(function(){
		$('.nav_menu_lessons').hide();
		$(this).next().stop().fadeIn('fast');
	});

	// hide menu button on page scroll
	var scroll_pos = 0;
	$(document).scroll(function() {
		scroll_pos = $(this).scrollTop();
		if(scroll_pos > 300) {
			$('.menubtn').css('visibility', 'hidden');
		} else {
			$('.menubtn').css('visibility', 'visible');
		}
	});

}

function createMenuLessons(){
	var menu = '<div id="menu">';
	menu += '<ul class="nav_menu_modules">';
	
	for(var i=0; i<FLVS.Sitemap.module.length; i++){
		
		if(FLVS.Sitemap.module[i].visible == 'true' || getCookie(settings.course_title + ' preview')){
			menu += '<li>';
			menu += '<a href="javascript:void(0);" class="modlink"><img src="../../global/images/home/icon_' + FLVS.Sitemap.module[i].icon + '.png"><span class="nav_num">' + FLVS.Sitemap.module[i].num + ' -</span> <span class="nav_title">' + FLVS.Sitemap.module[i].title + '</span></a>';
				
			// Lessons
			menu += '<ul class="nav_menu_lessons mod'+(i)+'">';
			var submenu = '';
			
			for(var j=0; j<FLVS.Sitemap.module[i].lesson.length; j++){
				var llink = FLVS.Sitemap.module[i].lesson[j].page[0].href;
				var ltitle = FLVS.Sitemap.module[i].lesson[j].title;
				var lnum = FLVS.Sitemap.module[i].lesson[j].num;
				var lmins = FLVS.Sitemap.module[i].lesson[j].time;
				var lpoints = FLVS.Sitemap.module[i].lesson[j].points;
				//llink = llink.replace("../../","");

				submenu += '<li>';
				
				//work around for current theme to allow alternate colors
				var mainWidth = $('#wrapper').css('width');
				if (mainWidth > '760px') {
					if(j == 0 || j== 2 || j==4 || j== 6 || j== 8 || j== 10 || j==12 || j==14 || j==16 || j==18 || j==20 || j==22){
						submenu += '<a href="'+llink+'"><span class="lesson_num">'+lnum+'</span>';
					} else {
						submenu += '<a href="'+llink+'" class="odd"><span class="lesson_num">'+lnum+'</span>';
					}
				} else if(mainWidth > '500px'){
					if(j == 0 || j== 2 || j==5 || j== 7 || j== 8 || j== 10 || j==13 || j==15 || j==16 || j==18 || j==21 || j==23){
						submenu += '<a href="'+llink+'"><span class="lesson_num">'+lnum+'</span>';
					} else {
						submenu += '<a href="'+llink+'" class="odd"><span class="lesson_num">'+lnum+'</span>';
					}
				} else {
					if(j%2 == 0){
						submenu += '<a href="'+llink+'"><span class="lesson_num">'+lnum+'</span>';
					} else {
						submenu += '<a href="'+llink+'" class="odd"><span class="lesson_num">'+lnum+'</span>';
					}
				}
				
				var minutes = 'mins';
				if(Number(FLVS.Sitemap.module[i].lesson[j].time) < 2){
					minutes = 'min';
				}
				var points = 'pts';
				if(Number(FLVS.Sitemap.module[i].lesson[j].points) < 2){
					points = 'pt';
				}		
				
				submenu += '<span class="lesson_title">'+ltitle+'</span><span class="lesson_nfo">'+lmins+' '+minutes+' | '+lpoints+' '+points+'</span></a>';
				submenu += '</li>';					
				
			}
				
			menu += submenu;
			menu += '</ul>';
			menu += '</li>';
		} // end if visible

	}
	
// Remove all modlinks from nav_menu_lessons
	$('#nav_menu').append(menu);
	$('.nav_menu_lessons .modlink').remove();

}

//-------------------need to see if the sitemap is ready, if not lets wait for the ajax to finish---------------------//
if ( FLVS.Sitemap ) {
	createIndexLessons();
	//console.log("hello - FLVS.Sitemap");
	
} else {
	$( document ).ajaxSuccess(function( event, xhr, settings ) {
	  if ( settings.url == '../../global/xml/sitemap.xml' ) {
		//console.log("hello - ajaxSuccess");
		createIndexLessons();
	  }
	});
}