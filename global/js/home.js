// ----------------------------------------------------------------------
// -- COURSE SPECIFIC JS:
// ----------------------------------------------------------------------
// -- NOTE: This is where you can add anything you need to do specifically to the course homepage, it will load lastly.
// -- ABOUT: This file will over-ride everything else, if you need to customize
// -- AUTHOR: Vaughn Thompson - WDS
// ======================================================================

function createIndex(){
	
	// screen size testing
	function windowSizer() {
		var winWidth = $(window).width();
		var winHeight = $(window).height();
		$('#winwidth').html(winWidth);
		$('#winheight').html(winHeight);
	}
	
	// create MODULES
	for(var i=0; i < FLVS.Sitemap.module.length; i++){ 
		var div = $(document.createElement('div')).attr('data-ref',FLVS.Sitemap.module[i].title).attr('id','menulink' + i);
		var modSelect = $(document.createElement('div')).addClass('moduleselect').attr('data-ref',i);
		var modIcon = $(document.createElement('div')).html('<img src="global/images/home/icon_' + FLVS.Sitemap.module[i].icon + '.png">').addClass('mod_icon');
		var modNum = $(document.createElement('span')).html(FLVS.Sitemap.module[i].num).addClass('mod_num');
		var modTitle = $(document.createElement('span')).html(FLVS.Sitemap.module[i].title).addClass('mod_title');
		$(div).append(modSelect);
		$(modSelect).append(modIcon).append(modNum).append(modTitle);
		$('#lesson_menu').append('<div class="div_lessons" id="mod' + i + '"></div>');
		
		// divide it all up, sort of, n-stuff
		$('#mod' + i).append('<div class="lessonDivL"><span class="lessonHead">Introduction</span><div id="introTxtDivL_' + i + '"></div></div>');
		$('#mod' + i + ' .lessonDivL').append('<span class="lessonHead">Lessons</span><div id="lessonTxtDivL_' + i + '"></div>');
		$('#mod' + i).append('<div class="lessonDivMid" id="lessonTxtDivMid_' + i + '"></div>');
		$('#mod' + i).append('<div class="lessonDivR"><div id="lessonTxtDivR_' + i + '"></div></div>');
		$('#mod' + i + ' .lessonDivR').append('<span class="lessonHead">Exam</span><div id="lessonExamTxt_' + i + '"></div>');
		
		// put 'em in the correct spot
		$('#modules').append(div);
		
		// create LESSONS
		var numLessons = FLVS.Sitemap.module[i].lesson.length;
		var divideIt = (numLessons / 3);
		var howMany = Math.round(divideIt);
		var col1 = howMany;						// Left column lessons
		var col2 = howMany + col1;				// Middle column lessons
		var col3 = numLessons;					// Right column lessons
		
		for(var j=0; j < numLessons; j++) { 
			var link = FLVS.Sitemap.module[i].lesson[j].page[0].href;
			link = link.replace('../../','');
			var lessonlink = $(document.createElement('a')).attr('href',link).addClass('lessonlink');
			var lessonnum = $(document.createElement('span')).html(FLVS.Sitemap.module[i].lesson[j].num).addClass('lessonNum');
			var lessontxt = $(document.createElement('span')).html(FLVS.Sitemap.module[i].lesson[j].title).addClass('lessonTitle');
			var dataNum = j + 1;
			
			// put them in the correct columns
			if ( i == 2 || i == 6 ) {										// module 3 and 7
				var examNum = (col3 - 1);
				if ( dataNum == 1 ) {										// introduction lesson
					$('#introTxtDivL_' + i).append(lessonlink); 
					$(lessonlink).append(lessonnum).append(lessontxt); 
				} else if ( dataNum > 1 && dataNum <= col1 ) {				// 1st column lessons
					$('#lessonTxtDivL_' + i).append(lessonlink); 
					$(lessonlink).append(lessonnum).append(lessontxt); 
				} else if ( dataNum > col1 && dataNum <= col2 ) {			// 2nd column lessons
					$('#lessonTxtDivMid_' + i).append(lessonlink); 
					$(lessonlink).append(lessonnum).append(lessontxt);
				} else if ( dataNum > col2 && dataNum < examNum ) {			// 3rd column lessons
					$('#lessonTxtDivR_' + i).append(lessonlink); 
					$(lessonlink).append(lessonnum).append(lessontxt);
				} else if ( dataNum >= examNum ) {							// exam lesson
					$('#lessonExamTxt_' + i).append(lessonlink); 
					$(lessonlink).append(lessonnum).append(lessontxt);
				}
			} else {
				if ( dataNum == 1 ) {										// introduction lesson
					$('#introTxtDivL_' + i).append(lessonlink); 
					$(lessonlink).append(lessonnum).append(lessontxt); 
				} else if ( dataNum > 1 && dataNum <= col1 ) {				// 1st column lessons
					$('#lessonTxtDivL_' + i).append(lessonlink); 
					$(lessonlink).append(lessonnum).append(lessontxt); 
				} else if ( dataNum > col1 && dataNum <= col2 ) {			// 2nd column lessons
					$('#lessonTxtDivMid_' + i).append(lessonlink); 
					$(lessonlink).append(lessonnum).append(lessontxt);
				} else if ( dataNum > col2 && dataNum < col3 ) {			// 3rd column lessons
					$('#lessonTxtDivR_' + i).append(lessonlink); 
					$(lessonlink).append(lessonnum).append(lessontxt);
				} else if ( dataNum == col3 ) {								// exam lesson
					$('#lessonExamTxt_' + i).append(lessonlink); 
					$(lessonlink).append(lessonnum).append(lessontxt);
				}
			}
			
		}
	}
	
	$('#modules').append('<div class="clear">&nbsp;</div>');
	
	// display lesson on click
	$('.moduleselect').click(function(fn){
		$(this).addClass('moduleSelected');
		
		$('.lesson_backdrop').show();
		var modpos = ($(this));
		var itsme = modpos.attr('data-ref');		
		
		// toggle lesson menu display
		if ($('#mod' + itsme).hasClass('opened')){
			$('#mod' + itsme).hide();
			$('#mod' + itsme).removeClass('opened');
		} else {
			if ($('.div_lessons').hasClass('opened')){
				$('.opened').hide();
				$('.div_lessons').removeClass('opened');
			}
			$('#mod' + itsme).toggle();
			$('#mod' + itsme).toggleClass('opened');
		}
		
		$('#lesson_menu').slideDown(500);
		
	});
	
	
	// FADE IN CONTENT and position the nav_menu
	$('body').css('visibility','visible').hide();
	$('body').delay(1000).fadeIn(1000, function(){
				
		// Navigation Position
		var pos = $('#menu_inner').offset();
		$('#nav_menu').css('left',pos.left+'px');
		
		$('main').delay(1000).fadeIn(1000);
				
		// Position Popup Menu
		$(window).on('resize',function(){
			var pos = $('#menu_inner').offset();
			$('#nav_menu').css('left',pos.left+'px');
					
		});
	});
	
	// Custom footer logo 
	$('#footer_copyright img').attr('src', 'global/images/global/flvs_logo_bw.png');
	$('#footer_copyright span').html('&copy; 2015 FLORIDA VIRTUAL SCHOOL');
	
	// get rid of lessons when click outside of them
	$('.lesson_backdrop').click(function (e) {
		$('.opened').fadeOut();
		$('.moduleSelected').removeClass('moduleSelected');
		$('.div_lessons').removeClass('opened');
		$('#lesson_menu').fadeOut();
		$('.lesson_backdrop').fadeOut();
	});
	
	// remove lesson popup on window resize
	$(window).resize(function() {
		/*$('.opened').fadeOut();
		$('.moduleSelected').removeClass('moduleSelected');
		$('.div_lessons').removeClass('opened');
		$('#lesson_menu').fadeOut();
		$('.lesson_backdrop').fadeOut();*/
		
		// screen size testing
		windowSizer();
	});
	
	// Create Popup Menu
	createMenu();
	
	// run window sizer
	windowSizer();
	
	// Event for Menu Button
	$('.menubtn, .menubtn_mobile').click(function(){
		$('.navnoheader').removeClass('navnohover');
		$('.nav_menu_lessons').hide();
		$('.moduleSelected').removeClass('moduleSelected');
		if(!$('#nav_menu').is(':visible')){
			$('body').append('<div class="menu_backdrop">&nbsp;</div>');
			$('.menu_backdrop').click(function(){
				$('#nav_menu').fadeToggle('fast');
				$(this).remove();
			});
		} else {
			$('.menu_backdrop').remove();
		}
				
		$('#nav_menu').fadeToggle('fast');
		
		//in case home nav menu is open
		$('.opened').fadeOut();
		$('.div_lessons').removeClass('opened');
		$('#lesson_menu').fadeOut();
		$('.lesson_backdrop').fadeOut();
				
	});
		
	// Event for Showing Menu Lessons
	$('.modlink').click(function(){
		$('.nav_menu_lessons').hide();
		$(this).next().stop().fadeIn('fast');
	});
}

function createMenu(){
	var menu = '<div id="menu">';
	menu += '<ul class="nav_menu_modules">';
	
	for(var i=0; i<FLVS.Sitemap.module.length; i++){
		
		if(FLVS.Sitemap.module[i].visible == 'true' || getCookie(settings.course_title + ' preview')){
			menu += '<li>';
			menu += '<a href="javascript:void(0);" class="modlink"><img src="global/images/home/icon_' + FLVS.Sitemap.module[i].icon + '.png"><span class="nav_num">' + FLVS.Sitemap.module[i].num + ' -</span> <span class="nav_title">' + FLVS.Sitemap.module[i].title + '</span></a>';
				
			// Lessons
			menu += '<ul class="nav_menu_lessons mod'+(i)+'">';
			var submenu = '';
			
			for(var j=0; j<FLVS.Sitemap.module[i].lesson.length; j++){
				var llink = FLVS.Sitemap.module[i].lesson[j].page[0].href;
				var ltitle = FLVS.Sitemap.module[i].lesson[j].title;
				var lnum = FLVS.Sitemap.module[i].lesson[j].num;
				var lmins = FLVS.Sitemap.module[i].lesson[j].time;
				var lpoints = FLVS.Sitemap.module[i].lesson[j].points;
				llink = llink.replace("../../","");

				submenu += '<li>';
				
				//work around for current theme to allow alternate colors
				var mainWidth = $('#wrapper').css('width');
				if (mainWidth > '760px') {
					if(j == 0 || j== 2 || j==4 || j== 6 || j== 8 || j== 10 || j==12 || j==14 || j==16 || j==18 || j==20 || j==22){
						submenu += '<a href="'+llink+'"><span class="lesson_num">'+lnum+'</span>';
					} else {
						submenu += '<a href="'+llink+'" class="odd"><span class="lesson_num">'+lnum+'</span>';
					}
				} else if(mainWidth > '500px'){
					if(j == 0 || j== 2 || j==5 || j== 7 || j== 8 || j== 10 || j==13 || j==15 || j==16 || j==18 || j==21 || j==23){
						submenu += '<a href="'+llink+'"><span class="lesson_num">'+lnum+'</span>';
					} else {
						submenu += '<a href="'+llink+'" class="odd"><span class="lesson_num">'+lnum+'</span>';
					}
				} else {
					if(j%2 == 0){
						submenu += '<a href="'+llink+'"><span class="lesson_num">'+lnum+'</span>';
					} else {
						submenu += '<a href="'+llink+'" class="odd"><span class="lesson_num">'+lnum+'</span>';
					}
				}
				
				var minutes = 'mins';
				if(Number(FLVS.Sitemap.module[i].lesson[j].time) < 2){
					minutes = 'min';
				}
				var points = 'pts';
				if(Number(FLVS.Sitemap.module[i].lesson[j].points) < 2){
					points = 'pt';
				}		
				
				submenu += '<span class="lesson_title">'+ltitle+'</span><span class="lesson_nfo">'+lmins+' '+minutes+' | '+lpoints+' '+points+'</span></a>';
				submenu += '</li>';					
				
			}
				
			menu += submenu;
			menu += '</ul>';
			menu += '</li>';
		} // end if visible

	}
	
// Remove all modlinks from nav_menu_lessons
	$('#nav_menu').append(menu);
	$('.nav_menu_lessons .modlink').remove();
}

//-------------------need to see if the sitemap is ready, if not lets wait for the ajax to finish---------------------//
if (FLVS.Sitemap){
	createIndex();	
} else {
	$( document ).ajaxSuccess(function( event, xhr, settings ) {
	  if (settings.url == 'global/xml/sitemap.xml' ) {
		createIndex();
	  }
	});
}
