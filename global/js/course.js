// ----------------------------------------------------------------------
// -- COURSE SPECIFI JS:
// ----------------------------------------------------------------------
// -- NOTE: This is where you can add anything you need to do specifically to the course, it will load lastly.
// -- ABOUT: THis file will over-ride everything else, if you need to customize
// -- AUTHOR: You - WDS
// ======================================================================

//console.log('course.js loaded');

if ( $('body').css('visibility','hidden') ) {
	$('body').css('visibility','visible');
}

// =======================================================
// Make sure tabs panel is as tall as the tabs block
// =======================================================
var tabblockheight = $('.tabs-block').height();
if (tabblockheight != null) {
	console.log('tab height: ' + tabblockheight + 'px');
	$('.tabs .panel-block .panel').css('min-height', tabblockheight + 'px');
}
