// JavaScript Document

$(document).ready(function(){

	// initialize
	var randomizer;
	var numCoins;
	var numHeads;
	var numTails;

	// display coins
	function coinGenerator(){

		numCoins = $('#howMany').val();

		if ( numCoins > 0 && numCoins < 201 ) {

			$('#coinHolder').empty(); // clear out coins

			for (var i = 0; i < numCoins; i++) {
				randomizer = Math.floor(Math.random() * 2) + 1;
				//console.log(randomizer);
				var showCoin = $(document.createElement('img')).attr('src','interactives/intmedia/coin' + randomizer + '.png');
				if ( randomizer == 1 ) {
					showCoin.addClass('heads');
				} else if ( randomizer == 2 ) {
					showCoin.addClass('tails');
				}

				$('#coinHolder').append(showCoin);
			}

		} else {
			alert('You entered an invalid number. Please enter a number between 1 and 200.');
		}

		numHeads = $('.heads').length;
		numTails = $('.tails').length;
		$('#cns_head').html(numHeads);
		$('#cns_tails').html(numTails);
		$('#resultsTxt').fadeIn();
	}


	// click generate button
	$('#btnGenerate').click(function(){
		coinGenerator();
	});


});